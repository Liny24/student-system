package com.learn.service;

import com.learn.pojo.Course;
import com.learn.pojo.Score;
import com.learn.pojo.Student;

import java.util.List;

public interface StudentService {

    //通过学号查询某个学生信息
    Student selectStu(String stuNo);

    //查询所有学生信息
    List<Student> selectAllStu();

    //计算某个学生的平均分
    double calStuAverage(String stuNo);

    //计算某个学生已修学分
    int calStuCredit(String stuNo);

    //查找学生有成绩的所有课程
    List<Course> queryAllCourse(String stuNo);

    //分页查找学生课程
    List<Course> querySomeCourse(List<Course> courseList, int pageNum, int offset);

    //查找学生的所有成绩（通过学号）
    List<Score> queryAllScoreByStuNo(String stuNo);

    //查找学生所有课程（不通过任何方式）
    List<Course> queryAllCourse();

    //  查找学生所有成绩，不通过任何方式
    List<Score> queryAllScore();

    //分页查找学生成绩
    List<Score> querySomeScore(List<Score> scoreList, int pageNum, int offset);

}
