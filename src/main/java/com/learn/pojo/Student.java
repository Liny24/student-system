package com.learn.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Date;
import java.text.SimpleDateFormat;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Student {

    private String stuNo;   //  学号
    private String stuName; //  姓名
    private Integer sex;    //  性别
    private Date birthday;  //  出生年月日
    private String nat;     //  民族
    private String classNo; //  班级号
    private Integer stuAge; //  年龄
    private String mail;    //  邮箱
    private String instituteNo; //  学院号
    private String address; //  地址


    //这里我改了返回值
    public String getBirthday() {
        SimpleDateFormat simple = new SimpleDateFormat("yyyy-MM-dd");
        String date = simple.format(birthday);
        return date;
    }
}
