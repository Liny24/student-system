package com.learn.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Course {

    String courseNo;
    String courseName;
    int credit;
    int courseHour;
    String priorCourse;


}
