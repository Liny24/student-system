package com.learn.pojo;

//这个是用于连接查询的pojo

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Score {
    String stuNo;
    String courseNo;
    String term;
    Integer score;
    String courseName;      //这是连接课程表的课程名

}
