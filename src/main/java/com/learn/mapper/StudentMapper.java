package com.learn.mapper;

import com.learn.pojo.Course;
import com.learn.pojo.Score;
import com.learn.pojo.Student;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

import java.util.List;


@Mapper
@Component
public interface StudentMapper {


    //通过学号查询某个学生信息
    Student selectStu(String stuNo);

    //查询所有学生信息
    List<Student> selectAllStu();

    //返回一个学生所有课程成绩的总和
    int calStuSum(String stuNo);

    //返回一个学生有成绩的课程数
    int getScoreNum(String stuNo);

    //计算某个学生已修学分
    int calStuCredit(String stuNo);

    //查找学生有成绩的所有课程（通过学号）
    List<Course> queryAllCourseByStuNo(String stuNo);

    //查找学生有成绩的所有课程（不通过任何方式）
    List<Course> queryAllCourse();

    //查找学生所有成绩
    List<Score> queryAllScoreByStuNo(String stuNo);

    //  查找学生所有成绩，不通过任何方式
    List<Score> queryAllScore();
}
