package com.learn.mapper;

import com.learn.pojo.ClassInfo;
import com.learn.pojo.Score;
import com.learn.pojo.Student;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

import java.util.List;

@Mapper
@Component
public interface ManageMapper {

    //查询某个班级号是否存在，如果存在就不能插入
    int isExistClassNo(String classNo);

    //插入班级信息
    void insertClassInfo(ClassInfo cla);

    //查询某个学号是否存在，如果存在就不能插入
    int isExistStuNo(String stuNo);

    //插入学生信息
    void insertStudentInfo(Student student);

    //插入学生成绩
    void insertStudentScore(Score score);

    //删除学生成绩
    void deleteStudentScore(Score score);

    //更新学生成绩
    void updateStudentScore(Score score);

    //更新学生信息
    void updateStudentInfo(Student student);

    //查询所有的班级号，以便学生信息插入
    List<String> queryAllClassNo();

    //查询所有的学院号
    List<String> queryAllInstituteNo();

    //班级人数加一
    void updateClassNum(String classNo);

    //按照学号删除学生信息
    void deleteStuByNo(String stuNo);

    //班级人数减一
    void updateClassNumM(String classNo);

    //根据学号删除成绩表
    void deleteScoreByNo(String stuNo);

    //查询所有学生信息
    List<Student> queryAllStudent();

    //通过学号找班级号
    String queryClassNoByStuNo(String stuNo);

}
