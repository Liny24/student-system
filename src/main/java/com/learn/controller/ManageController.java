package com.learn.controller;

import com.learn.pojo.ClassInfo;
import com.learn.pojo.Score;
import com.learn.pojo.Student;
import com.learn.service.serviceImpl.ManageServiceImpl;
import com.learn.service.serviceImpl.StudentServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Controller
public class ManageController {
    @Autowired
    StudentServiceImpl studentService;

    @Autowired
    ManageServiceImpl manageService;

    @RequestMapping(value = "/insertClass", method = RequestMethod.POST)
    public ModelAndView insertClass(@RequestParam("classNo") String classNo,
                                    @RequestParam("className") String className,
                                    @RequestParam("institute") String institute,
                                    @RequestParam("grade") int grade,
                                    ModelAndView mv, HttpSession session) {
        //先检查是否存在相同的班级号
        int isExist = manageService.isExistClassNo(classNo);
        mv.addObject("userType", session.getAttribute("userType"));
        mv.addObject("userName", session.getAttribute("stuNo"));
        if (isExist == 1) {           //存在，不能插入
            mv.addObject("isExist", 1);
            mv.setViewName("welcome");
        } else {
            ClassInfo cla = new ClassInfo(classNo, className, institute, grade);
            manageService.insertClassInfo(cla);
            mv.addObject("success", 1);
            mv.setViewName("welcome");
        }
        return mv;
    }

    //get方式跳转，不用接受数据
    @RequestMapping(value = "/insertClass", method = RequestMethod.GET)
    public ModelAndView insertClass(ModelAndView mv, HttpSession session) {
        mv.addObject("userType", session.getAttribute("userType"));
        mv.addObject("userName", session.getAttribute("stuNo"));
        mv.setViewName("welcome");
        return mv;
    }

    //跳转函数，跳转到学生查询页面都要通过这个函数获取可供选择的班级号
    @RequestMapping(value = "/queryStuInfo", method = RequestMethod.GET)
    public ModelAndView queryStuInfo(ModelAndView mv, HttpSession session) {
        mv.addObject("userType", session.getAttribute("userType"));
        mv.addObject("userName", session.getAttribute("stuNo"));
        List<Student> studentList = manageService.queryAllStudent();
        System.out.println(studentList);
        mv.addObject("studentList", studentList);
        return mv;
    }


    //跳转函数，跳转到学生插入页面都要通过这个函数获取可供选择的班级号
    @RequestMapping("/toInsertStuInfo")
    public String toInsertStuInfo(Model model, HttpSession session) {
        model.addAttribute("userType", session.getAttribute("userType"));
        model.addAttribute("userName", session.getAttribute("stuNo"));

        List<String> str_stuNo = new ArrayList<String>();

        str_stuNo = manageService.queryAllClassNo();      //获取所有的班级号

        model.addAttribute("classNo", str_stuNo);

        return "insertStuInfo";
    }


    @RequestMapping(value = "/addStuInfo", method = RequestMethod.POST)
    public ModelAndView insertStu(@RequestParam("stuNo") String stuNo,
                                  @RequestParam("stuName") String stuName,
                                  @RequestParam("nat") String nat,
                                  @RequestParam("birthday") String birthday,
                                  @RequestParam("sex") Integer sex,
                                  @RequestParam("classNo") String classNo,
                                  @RequestParam("stuAge") Integer stuAge,
                                  @RequestParam("mail") String mail,
                                  @RequestParam("instituteNo") String instituteNo,
                                  @RequestParam("address") String address,
                                  ModelAndView mv, HttpSession session) throws ParseException {
        mv.addObject("userType", session.getAttribute("userType"));
        mv.addObject("userName", session.getAttribute("stuNo"));

        //邮箱格式规范
        String regEx_mail = "^\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*$";
        Pattern pattern_mail = Pattern.compile(regEx_mail);
        Matcher matcher_mail = pattern_mail.matcher(regEx_mail);
        //日期格式规范
        String regEx_birthday = "^(19|20)\\d{2}\\-((0?[1-9])|(1[0-2]))\\-((0?[1-9])|([1-2]\\d)|3[01])$";
        Pattern pattern_birthday = Pattern.compile(regEx_birthday);
        Matcher matcher_birthday = pattern_birthday.matcher(regEx_birthday);

        //获取所有的班级号
        List<String> str_StuNo = new ArrayList<String>();
        str_StuNo = manageService.queryAllClassNo();
        mv.addObject("classNo", str_StuNo);
        //获取所有的所在系
        List<String> str_instituteNo = new ArrayList<String>();
        manageService.queryAllInstituteNo();
        mv.addObject("instituteNo", str_instituteNo);

        int isExist = manageService.isExistStuNo(stuNo);

        //判断是否存在
        if (isExist != 0) {
            mv.addObject("isExist", 1);
        } else if (stuName == null || stuName.equals("")) {
            mv.addObject("nameNull", 1);
        } else if (nat == null) {
            mv.addObject("natNull", 1);
        } else if (!matcher_birthday.matches()) {
            mv.addObject("birthdayNull", 1);
        } else if (sex == 0) {
            mv.addObject("sexNull", 1);
        } else if (classNo == null) {
            mv.addObject("classNoNull", 1);
        } else if (stuAge <= 0 || stuAge > 100) {
            mv.addObject("stuAgeNull", 1);
        } else if (!matcher_mail.matches()) {
            mv.addObject("mailFail", 1);
        } else {
            //将生日字符串转换成date类型
            SimpleDateFormat simple = new SimpleDateFormat("yyyy-MM-dd");
            Date date = new Date(simple.parse(birthday).getTime());
            Student stu = new Student(stuNo, stuName, sex, date, nat, classNo, stuAge, mail, instituteNo, address);
            manageService.insertStudentInfo(stu);
            mv.addObject("success", 1);
        }
        mv.setViewName("addStuInfo");
        return mv;
    }

    //  跳转函数，跳转到学生修改页面都要通过这个函数获取可供选择的班级号
    @RequestMapping("/toUpdataStuInfoPage")
    public String toUpdataStuInfo(Model model, HttpSession session) {
        model.addAttribute("userType", session.getAttribute("userType"));
        model.addAttribute("userName", session.getAttribute("stuNo"));

        //获取所有的班级号
        List<String> str_StuNo = new ArrayList<String>();
        str_StuNo = manageService.queryAllClassNo();

        //获取所有的所在系
        List<String> str_InstituteNo = new ArrayList<String>();
        manageService.queryAllInstituteNo();

        model.addAttribute("classNo", str_StuNo);

        model.addAttribute("instituteNo", str_InstituteNo);
        return "updataStuInfo";
    }

    @RequestMapping(value = "/updataStu", method = RequestMethod.POST)
    public ModelAndView UpdataStu(@RequestParam("stuNo") String stuNo,
                                  @RequestParam("stuName") String stuName,
                                  @RequestParam("nat") String nat,
                                  @RequestParam("birthday") String birthday,
                                  @RequestParam("sex") Integer sex,
                                  @RequestParam("classNo") String classNo,
                                  @RequestParam("stuAge") Integer stuAge,
                                  @RequestParam("mail") String mail,
                                  @RequestParam("instituteNo") String instituteNo,
                                  @RequestParam("address") String address,
                                  ModelAndView mv, HttpSession session) throws ParseException {
        mv.addObject("userType", session.getAttribute("userType"));
        mv.addObject("userName", session.getAttribute("stuNo"));

        List<String> str = new ArrayList<String>();
        str = manageService.queryAllClassNo();      //获取所有的班级号
        mv.addObject("classNo", str);

        //将生日字符串转换成date类型
        SimpleDateFormat simple = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date(simple.parse(birthday).getTime());
        Student stu = new Student(stuNo, stuName, sex, date, nat, classNo, stuAge, mail, instituteNo, address);
        manageService.updateStudentInfo(stu);
        mv.addObject("success", 1);

        mv.setViewName("updataStuInfo");
        return mv;
    }

    @RequestMapping(value = "/insertStuScore", method = RequestMethod.POST)
    public ModelAndView insertStuScore(@RequestParam("stuNo") String stuNo,
                                       @RequestParam("courseNo") String courseNo,
                                       @RequestParam("term") String term,
                                       @RequestParam("score") Integer score,

                                       ModelAndView mv, HttpSession session) {
        mv.addObject("userType", session.getAttribute("userType"));
        mv.addObject("userName", session.getAttribute("stuNo"));

        int existStuNo = manageService.isExistStuNo(stuNo);

        if (existStuNo == 1) {
            if (stuNo == null || stuNo.length() == 0) {
                mv.addObject("stuNoNull", 1);
            } else if (courseNo == null || courseNo.length() == 0) {
                mv.addObject("courseNoNull", 1);
            } else if (term == null || term.length() == 0) {
                mv.addObject("termNull", 1);
            } else if (score < 0 || score > 100) {
                mv.addObject("scoreError", 1);
            }
            manageService.insertStudentScore(new Score(stuNo, courseNo, term, score, null));
            mv.addObject("success", 1);
        } else {
            mv.addObject("notExist", 1);
        }

        mv.setViewName("addStuScore");
        return mv;
    }

    @RequestMapping(value = "/deleteStuScore", method = RequestMethod.POST)
    public ModelAndView deleteStuScore(@RequestParam("stuNo") String stuNo,
                                       @RequestParam("courseNo") String courseNo,
                                       ModelAndView mv, HttpSession session) {

        mv.addObject("userType", session.getAttribute("userType"));
        mv.addObject("userName", session.getAttribute("stuNo"));

        int existStuNo = manageService.isExistStuNo(stuNo);

        if (existStuNo != 0) {
            if (stuNo == null || stuNo.length() == 0) {
                mv.addObject("stuNoNull", 1);
            } else if (courseNo == null || courseNo.length() == 0) {
                mv.addObject("courseNoNull", 1);
            }
            //  删除学生成绩信息
            manageService.deleteStudentScore(new Score(stuNo, courseNo, null, null, null));
            mv.addObject("success", 1);
        } else {
            mv.addObject("notExist", 1);
        }
        mv.setViewName("deleteStuScore");
        return mv;
    }

    @RequestMapping(value = "/updataStuScore", method = RequestMethod.POST)
    public ModelAndView updataStuScore(@RequestParam("stuNo") String stuNo,
                                       @RequestParam("courseNo") String courseNo,
                                       @RequestParam("term") String term,
                                       @RequestParam("score") Integer score,
                                       ModelAndView mv, HttpSession session) {
        mv.addObject("userType", session.getAttribute("userType"));
        mv.addObject("userName", session.getAttribute("stuNo"));

        int existStuNo = manageService.isExistStuNo(stuNo);

        if (existStuNo != 0) {
            if (stuNo == null || stuNo.length() == 0) {
                mv.addObject("stuNoNull", 1);
            } else if (courseNo == null || courseNo.length() == 0) {
                mv.addObject("courseNoNull", 1);
            } else if (term == null || term.length() == 0) {
                mv.addObject("termNull", 1);
            } else if (score < 0 || score > 100) {
                mv.addObject("scoreError", 1);
            }
            //  更新学生成绩
            manageService.updateStudentScore(new Score(stuNo, courseNo, term, score, null));
            mv.addObject("success", 1);
        } else {
            mv.addObject("notExist", 1);
        }
        mv.setViewName("updataStuScore");
        return mv;
    }

    //  获取当前页面的值
    @RequestMapping(value = "/getValue")
    @ResponseBody
    public String getValue(@RequestParam("stuNo") String stuNo) {
        System.out.println(stuNo);
        return "helloWorld";
    }

    //deleteStu显示函数
    @RequestMapping(value = "/deleteStu/{pageNum}", method = RequestMethod.GET)
    public ModelAndView deleteStu(@PathVariable(value = "pageNum") int pageNum,
                                  ModelAndView mv, HttpSession session) {
        mv.addObject("userType", session.getAttribute("userType"));
        mv.addObject("userName", session.getAttribute("stuNo"));
        int offset = 5;     //每页显示的数量
        int total = manageService.queryAllStudent().size(); //学生信息总数
        int totalPage = total / offset;
        if (total % offset != 0) {
            totalPage++;
        }
        if (totalPage == 0) {
            totalPage = 1;
        }
        mv.addObject("totalPage", totalPage);
        mv.addObject("pageNum", pageNum);
        List<Student> studentList = new ArrayList<Student>();
        studentList = manageService.querySomeStudent(manageService.queryAllStudent(),
                pageNum, offset);
        mv.addObject("studentList", studentList);
        mv.setViewName("deleteStu");
        return mv;
    }

    //删除函数
    @RequestMapping(value = "/delete/{stuNo}", method = RequestMethod.GET)
    public ModelAndView delete(@PathVariable("stuNo") String stuNo,
                               ModelAndView mv, HttpSession session) {
        manageService.deleteStuByNo(stuNo);
        mv.setViewName("redirect:/deleteStu/1"); //转发请求
        return mv;
    }

    //根据输入的学号删除学生个人信息
    @RequestMapping(value = "/deleteStuInfo", method = RequestMethod.POST)
    public ModelAndView deleteStuInfo(@RequestParam("stuNo") String stuNo,
                                      @RequestParam("stuName") String stuName,
                                      ModelAndView mv, HttpSession session) {
        mv.addObject("userType", session.getAttribute("userType"));
        mv.addObject("userName", session.getAttribute("stuNo"));

        int existStuNo = manageService.isExistStuNo(stuNo);

        if (existStuNo != 0) {
            if (stuNo == null || stuNo.length() == 0) {
                mv.addObject("stuNoNull", 1);
            }else if(stuName == null || stuName.length()==0){
                mv.addObject("stuNameNull", 1);
            }
            //  删除学生信息
            manageService.deleteStuByNo(stuNo);
            mv.addObject("success", 1);
        } else {
            mv.addObject("notExist", 1);
        }
        mv.setViewName("deleteStuInfo");
        return mv;
    }


    // 跳转学生个人信息查询
    @RequestMapping(value = "/toQueryStuInfoPage")
    public String toCheckStuInfoPage(Model model, HttpSession session) {
        model.addAttribute("userType", session.getAttribute("userType"));
        model.addAttribute("userName", session.getAttribute("stuNo"));
        List<Student> studentList = manageService.queryAllStudent();
        model.addAttribute("studentList", studentList);
        return "queryStuInfo";
    }

    // 跳转学生成绩
    @RequestMapping(value = "/toCheckStuScorePage")
    public String toCheckStuScorePage(Model model, HttpSession session) {
        model.addAttribute("userType", session.getAttribute("userType"));
        model.addAttribute("userName", session.getAttribute("stuNo"));

        //  获取所有学生信息
        List<Student> list_stu = manageService.queryAllStudent();
        model.addAttribute("list_stu", list_stu);

        //  获取所有学生的学号
        List<String> list_stuNo = new ArrayList<>();
        for (Student student : list_stu) {
            list_stuNo.add(student.getStuNo());
        }
        //  获取所有学生的信息和成绩
        List<Score> scoreList = studentService.queryAllScore();

        //  成绩
        model.addAttribute("scoreList", scoreList);
        return "checkScore";
    }

    // 跳转学生培养计划
    @RequestMapping(value = "/toCheckCoursePage")
    public String toCheckCoursePage(Model model, HttpSession session) {
        model.addAttribute("userType", session.getAttribute("userType"));
        model.addAttribute("userName", session.getAttribute("stuNo"));
        return "checkCourse";
    }

    @RequestMapping(value = "/toAddStuScorePage")
    public String toAddStuScorePage(Model model, HttpSession session) {
        model.addAttribute("userType", session.getAttribute("userType"));
        model.addAttribute("userName", session.getAttribute("stuNo"));

        List<Score> scores = studentService.queryAllScore();
        model.addAttribute("scores", scores);

        return "addStuScore";
    }


    @RequestMapping(value = "/toDeleteStuScorePage")
    public String toDeleteStuScorePage(Model model, HttpSession session) {
        model.addAttribute("userType", session.getAttribute("userType"));
        model.addAttribute("userName", session.getAttribute("stuNo"));

        return "deleteStuScore";
    }

    @RequestMapping(value = "/toUpdataStuScorePage")
    public String toUpdataStuScorePage(Model model, HttpSession session) {
        model.addAttribute("userType", session.getAttribute("userType"));
        model.addAttribute("userName", session.getAttribute("stuNo"));

        return "updataStuScore";
    }


    // 跳转welcome页
    @RequestMapping(value = {"/toWelcomePage"})
    public String toWelcomePage(Model model, HttpSession session) {
        model.addAttribute("userType", session.getAttribute("userType"));
        model.addAttribute("userName", session.getAttribute("stuNo"));
        return "welcome";
    }


    // 跳转添加学生个人信息页面
    @RequestMapping(value = "/toAddStuInfoPage")
    public String toAddStuInfoPage(Model model, HttpSession session) {
        model.addAttribute("userType", session.getAttribute("userType"));
        model.addAttribute("userName", session.getAttribute("stuNo"));
        return "addStuInfo";
    }

    @RequestMapping(value = "/toDeleteStuInfoPage")
    public String toDeleteStuInfoPage(Model model, HttpSession session) {
        model.addAttribute("userType", session.getAttribute("userType"));
        model.addAttribute("userName", session.getAttribute("stuNo"));
        return "deleteStuInfo";
    }


}
